<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BookController;
use App\Http\Controllers\AuthorController;
use App\Http\Controllers\CategoryController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

//* BOOKS - API/books

//? GET /books → Get all books
Route::get('/books/all', [BookController::class, "getAllBooks"]);

//? GET /books → Get all books with pagination
Route::get("/books", [BookController::class, "getBooks"]);

//? GET /books/{id} → Get a book
Route::get('/books/{id}', [BookController::class, "getBook"]);

//? POST /books → Create a book
Route::post('/books', [BookController::class, "createBook"]);

//? PUT /books/{id} → Update a book
Route::put('/books/{id}', function () {
    return 'This is a book update with id';
});

//? PATCH /books/{id} → Update a book
Route::patch('/books/{id}', function () {
    return 'This is a book patch with id';
});

//? DELETE /books/{id} → Delete a book
Route::delete('/books/{id}', function () {
    return 'This is a book delete with id';
});

//* CATEGORIES - API/categories

//? GET /categories → Get all categories
Route::get('categories/all', [CategoryController::class, 'getAllCategories']);

//? GET /categories → Get all categories with pagination
Route::get('/categories', [CategoryController::class, 'getCategories']);

//? GET /categories/{id} → Get a category
Route::get('/categories/{id}', [CategoryController::class, 'getCategory']);

//? PUT /categories/{id} → Update a category
Route::put('/categories/{id}', [CategoryController::class, 'updateCategory']);

//? POST /categories → Create a category
Route::post('categories', [CategoryController::class, 'createCategory']);


//* AUTHORS - API/authors

//? GET /authors → Get all authors
Route::get('authors/all', [AuthorController::class, 'getAllAuthors']);

//? GET /authors → Get all authors with pagination
Route::get('/authors', [AuthorController::class, 'getAuthors']);

//? GET /authors/{id} → Get an author
Route::get('/authors/{id}', [AuthorController::class, 'getAuthor']);

//? PUT /authors/{id} → Update an author
Route::put('/authors/{id}', [AuthorController::class, 'updateAuthor']);

//? POST /authors → Create an author
Route::post('authors', [AuthorController::class, 'createAuthor']);
