<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookAuthorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('book_author', function (Blueprint $table) {
            $table->id();
            $table->foreignId('id_book')->constrained('books')->onDelete('cascade')->onUpdate('cascade');
            $table->foreignId('id_author')->constrained('authors')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
            $table->unique(['id_book', 'id_author']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('book_author');
    }
}
