<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('books', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->string('description', 512)->nullable(true);
            $table->date('date_published');
            $table->string('ISBN', 18)->unique();
            $table->float('price', 10, 2);
            $table->string('image_url');
            //? Propiedad de disponibilidad del libro
            $table->enum('status', ['stock', 'out_of_stock', 'available_soon']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('books');
    }
}
