<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Book;
use Illuminate\Support\Facades\Validator;

class BookController extends Controller
{

    public function getAllBooks()
    {
        $books = Book::with('categories')->get();
        return response()->json($books, 200);
    }

    public function getBooks(Request $request)
    {
        $per_page = $request->query("per_page", 10);
        $category_id = $request->query("category_id");
        $author_id = $request->query("author_id");
        $order_by = $request->query("order_by", 'title');
        $order_direction = $request->query("order_direction", 'asc');
        $title = $request->query("title");

        $query = Book::query();

        if ($category_id) {
            $query->whereHas('categories', function ($query) use ($category_id) {
                $query->where('categories.id', $category_id);
            });
        }

        if ($author_id) {
            $query->whereHas('authors', function ($query) use ($author_id) {
                $query->where('authors.id', $author_id);
            });
        }

        if ($title) {
            $query->where('title', 'like', "%{$title}%");
        }

        //? Validate order_by field
        $validOrderFields = ['title', 'date_published', 'price'];
        if (in_array($order_by, $validOrderFields)) {
            $query->orderBy($order_by, $order_direction);
        }

        $books = $query->with('categories')->paginate($per_page);

        return response()->json($books, 200);
    }

    public function getBook($id)
    {
        $book = Book::with('categories', 'authors')->find($id);

        if (!$book) {
            return response()->json([
                "message" => "Book not found",
                "status" => 404,
            ], 404);
        }

        return response()->json($book, 200);
    }


    public function createBook(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'description' => 'required',
            'date_published' => 'required|date',
            'ids_categories' => 'required|array',
            'ids_categories.*' => 'exists:categories,id',
            'ids_authors' => 'required|array',
            'ids_authors.*' => 'exists:authors,id',
            'ISBN' => 'required|unique:books',
            'price' => 'required|numeric',
            'image_url' => 'required|url',
            'status' => 'required|in:stock,out_of_stock,available_soon',
        ]);

        if ($validator->fails()) {
            return response()->json([
                "message" => "Validation error",
                "errors" => $validator->errors(),
                "status" => 400,
            ], 400);
        }

        $book = Book::create([
            "title" => $request->title,
            "description" => $request->description,
            "ISBN" => $request->ISBN,
            "price" => $request->price,
            "image_url" => $request->image_url,
            "status" => $request->status,
            "date_published" => $request->date_published,
        ]);

        if (!$book) {
            return response()->json([
                "message" => "Error creating book",
                "errors" => $validator->errors(),
                "status" => 500,
            ], 500);
        }

        //? Attach categories and authors to the book -> Many to Many relationship
        $book->categories()->attach($request->ids_categories);
        $book->authors()->attach($request->ids_authors);

        return response()->json([
            "book" => $book,
            "status" => 201,
        ], 201);
    }

    //TODO: Implement the updateBook method


    //TODO: Implement the deleteBook method

}
