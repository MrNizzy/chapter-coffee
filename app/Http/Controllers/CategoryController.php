<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use Illuminate\Support\Facades\Validator;

class CategoryController extends Controller
{

    public function getAllCategories()
    {
        $categories = Category::all();
        return response()->json($categories, 200);
    }

    public function getCategories(Request $request)
    {
        $perPage = $request->query('per_page', 10);
        $categories = Category::paginate($perPage);
        return response()->json($categories, 200);
    }

    public function getCategory($id)
    {
        $category = Category::find($id);

        if (!$category) {
            return response()->json([
                "message" => "Category not found",
                "status" => 404,
            ], 404);
        }

        return response()->json($category, 200);
    }


    public function createCategory(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
        ]);

        //? Validate the request body fields
        if ($validator->fails()) {
            return response()->json([
                "message" => "Validation error",
                "errors" => $validator->errors(),
                "status" => 400,
            ], 400);
        }

        //? Create a new category
        $category = Category::create([
            "name" => $request->name,
        ]);

        if (!$category) {
            return response()->json([
                "message" => "Error creating category",
                "status" => 500,
            ], 500);
        }

        return response()->json([
            "category" => $category,
            "status" => 201,
        ], 201);
    }


    //TODO: Implement the updateCategory method
    public function updateCategory(Request $request, $id)
    {
        $category = Category::find($id);

        if (!$category) {
            return response()->json([
                "message" => "Category not found",
                "status" => 404,
            ], 404);
        }

        $validator = Validator::make($request->all(), [
            'name' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                "message" => "Validation error",
                "errors" => $validator->errors(),
                "status" => 400,
            ], 400);
        }

        //? Update the category
        $category->name = $request->name;
        $category->save();

        return response()->json([
            "category" => $category,
            "status" => 200,
        ], 200);
    }

    //TODO: Implement the deleteCategory method
}
