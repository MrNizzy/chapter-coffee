<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Author;
use Illuminate\Support\Facades\Validator;

class AuthorController extends Controller
{
    public function getAllAuthors()
    {
        $authors = Author::with('books')->get();
        return response()->json($authors, 200);
    }

    public function getAuthors(Request $request)
    {
        $per_page = $request->query('per_page', 10);
        $authors = Author::with('books')->paginate($per_page);
        return response()->json($authors, 200);
    }

    public function getAuthor($id)
    {
        $author = Author::with('books')->find($id);

        if (!$author) {
            return response()->json([
                "message" => "Author not found",
                "status" => 404,
            ], 404);
        }

        return response()->json($author, 200);
    }

    public function createAuthor(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'nationality' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                "message" => "Validation error",
                "errors" => $validator->errors(),
                "status" => 400,
            ], 400);
        }

        $author = Author::create([
            "name" => $request->name,
            "nationality" => $request->nationality,
        ]);

        if (!$author) {
            return response()->json([
                "message" => "Error creating author",
                "status" => 500,
            ], 500);
        }

        return response()->json([
            "author" => $author,
            "status" => 201,
        ], 201);
    }

    //TODO: Implement the updateAuthor method
    public function updateAuthor(Request $request, $id)
    {
        $author = Author::find($id);

        if (!$author) {
            return response()->json([
                "message" => "Author not found",
                "status" => 404,
            ], 404);
        }

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'nationality' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                "message" => "Validation error",
                "errors" => $validator->errors(),
                "status" => 400,
            ], 400);
        }

        //? Update the author
        $author->name = $request->name;
        $author->nationality = $request->nationality;
        $author->save();

        return response()->json([
            "author" => $author,
            "status" => 200,
        ], 200);

    }


    //TODO: Implement the deleteAuthor method

}
