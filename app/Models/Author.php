<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Author extends Model
{
    use HasFactory;

    protected $table = "authors";

    /**
     * The Author model represents an author in the application.
     *
     * @property string $name The name of the author.
     * @property string $nationality The nationality of the author.
     */
    protected $fillable = [
        "name",
        "nationality",
    ];

    /**
     * Get the authors associated with the book.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function books()
    {
        return $this->belongsToMany(Book::class, 'book_author', 'id_author', 'id_book');
    }

}
