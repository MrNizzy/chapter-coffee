<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;

    protected $table = "categories";

    /**
     * The Category model represents a category in the application.
     *
     * @property string $name The name of the category.
     */
    protected $fillable = [
        "name",
    ];

    /**
     * Get the books associated with the category.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function books()
    {
        return $this->belongsToMany(Book::class, 'book_category', 'id_category', 'id_book');
    }
}
