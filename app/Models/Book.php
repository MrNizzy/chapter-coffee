<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    use HasFactory;

    protected $table = "books";

    protected $fillable = [
        "title",
        "description",
        "date_published",
        "ISBN",
        "price",
        "image_url",
        "status",
    ];

    public function categories()
    {
        return $this->belongsToMany(Category::class, 'book_category', 'id_book', 'id_category');
    }

    public function authors()
    {
        return $this->belongsToMany(Author::class, 'book_author', 'id_author', 'id_book');
    }
}
